list_empty([], true).

minRek([N|Rest], X) :- list_empty(Rest,true), X =< N.
minRek([N|Rest], X) :- minRek(Rest, X), X =< N.
min(List, X) :- member(X,List), minRek(List, X).

memberTest([N|Rest],X) :- member(X,[N|Rest]).

/* use this program on itself ... */
 go :- write("min([1,2,3,4,5,6],X)"), min([1,2,3,4,5,6],X), write(' --> '), write(X), write("\nmin([5,6],X)"), min([5,6],Y), write(' --> '), write(Y).