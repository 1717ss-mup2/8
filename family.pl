male(tom).
male(bernd).
male(klaus).
male(simon).
male(david).
male(hugo).
female(anna).
female(susi).
female(karin).
female(klara).
/* likes(X, Y) - X mag Y */
likes(tom, dogs).
likes(anna, tom).
likes(hugo, wine).
likes(susi, dogs).
likes(simon, dogs).
likes(simon, anna).
likes(klaus, X):-likes(X, dogs).
likes(X,Y):-parent(X,Y); parent(Y,X).
likes(X, Y) :- X \= Y, (parents(X,Y); parents(Y,X)).
likes(klaus, Y) :- likes(Y,dog).
/* parent(X, Y) - X ist Elternteil von Y*/
parent(anna, bernd).
parent(klaus, tom).
parent(klaus, hugo).
parent(klaus, susi).
parent(simon, david).
parent(simon, klara).
parent(karin, david).
parent(karin, hugo).
parent(karin, susi).
parent(karin, klara).

mother(X,Y) :- parent(X,Y), female(X).
father(X,Y) :- parent(X,Y), female(X).
parents(X,Y) :- parent(X,Z), parent(Y,Z), X \= Y.
brother(X,Y) :- male(X), parent(A,X), parent(A,Y), parent(B,X), parent(B,Y), A \= B, X \= Y.
sister(X,Y) :- female(X), parent(A,X), parent(A,Y), parent(B,X), parent(B,Y), A \= B, X \= Y.
halfSibling(X,Y) :- parent(A,X), parent(A,Y), parent(B,X), parent(C,Y), A \= B, A \= C, B \= C, X \= Y.
son(X,Y) :- parent(Y,X), male(X).
daughter(X,Y) :- parent(Y,X), female(X).